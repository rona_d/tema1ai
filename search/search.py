# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
import random

###################
# Clasa ce contine informatiile despre starea nodului, actiunea ce s-a
# realizat pentru ajungerea la acel nod si costul curent al drumului
# dar si parintele acelui nod
############################################################
class Node:

    def __init__(self, state, action, cost, parent):
        self.state = state
        self.parent = parent
        self.action = action
        self.path_cost = cost

    def getState(self):
        return self.state

    def getParent(self):
        return self.parent

    def getAction(self):
        return self.action

    def getCost(self):
        return self.path_cost

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]



def depthFirstSearch(problem):

    frontier = util.Stack()     #frontiera implementata cu o stiva
    startState = problem.getStartState()        #starea initiala
    node = Node(startState, None, None, None)   #nodul starii initiale
    frontier.push(node)
    exploredSet = []
    actions = []
    while True:
        if frontier.isEmpty():      #daca frontiera este goala s-a ajuns la finalul algoritmului si nu s-a gasit niciun drum --> returnam None
            return None

        leaf = frontier.pop()       #scoatem cate un nod din frontiera si il luam ca si frunza

        if(problem.isGoalState(leaf.getState())):       #daca starea curenta este goal state se reconstruieste drumul parcurs pana la starea curenta de la cea
            while (not(leaf.getParent() == None)):      #initiala si se inverseaza lista de actiuni deoarece parcurgerea se face de la goal la startState
                actions.append(leaf.getAction())
                leaf = leaf.getParent()
            actions.reverse()
            break
        exploredSet.append(leaf.getState())     #introducem starea curenta in lista de noduri expandate (expandam nodul curent)

        #pentru fiecare succesor al starii curente se creeaza un nou nod si se verifica daca exista in frontiera sau in lista de noduri deja explorate,
        #iar daca e valid se introduce noul nod in frontiera

        for succ in problem.getSuccessors(leaf.getState()):
            newNode = Node(succ[0], succ[1], succ[2], leaf)
            if(not(frontier.list.__contains__(newNode) or exploredSet.__contains__(newNode.getState()))):
                frontier.push(newNode)
    return actions


def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"
    frontier = util.Queue()     #frontiera initializata ca si coada
    exploredSet = []            #lista cu noduri explorate
    actions = []
    startNode = Node(problem.getStartState(), None, 0, None)        #nodul starii initiale
    frontier.push(startNode)
    ok = True
    while True:
        if (frontier.isEmpty()):        #daca frontiera este goala s-a ajuns la finalul algoritmului si nu s-a gasit niciun drum --> returnam None
            return None

        leaf = frontier.pop()   #scoatem din frontiera un nod si il exploram

        if (problem.isGoalState(leaf.getState())):          #daca starea curenta este goal state se reconstruieste drumul parcurs pana la starea curenta de la cea
            while (not (leaf.getParent() == None)):         #initiala si se inverseaza lista de actiuni deoarece parcurgerea se face de la goal la startState
                actions.append(leaf.getAction())
                leaf = leaf.getParent()
            actions.reverse()
            return actions

        exploredSet.append(leaf.getState())     #adaugam nodul curent in lista de noduri explorate


        #pentru fiecare succesor al nodului curent creez un nou nod si verificam daca se afla in frontiera sau in lista de noduri explorate
        #daca nu se afla in ele adaugam nodul in frontiera

        for succ in problem.getSuccessors(leaf.getState()):
            newNode = Node(succ[0], succ[1], succ[2], leaf)
            if (not (exploredSet.__contains__(newNode.getState()))):
                for elem in frontier.list:                          #parcurg elementele din frontiera si verific daca exista egalitati intre stari (se putea mai scurt)
                    if (elem.getState() == newNode.getState()):
                        ok = False
                if ok:
                    frontier.push(newNode)
            ok = True

def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"

    node = Node(problem.getStartState(), None, 0, None) #nodul de start
    frontier = util.PriorityQueue()     #initializam frontiera ca o coada de prioritati unde prioritatea reprezinta costul drumului curent
    explored = []
    frontier.push(node, node.path_cost)
    actions = []
    ok = True

    while True:
        if(frontier.isEmpty()):     #daca frontiera este goala am ajuns la final si nu am gasit niciun drum valid
            return None
        leaf = frontier.pop()       #scoatem un nod din lista de prioritati(cel cu costul cel mai mic) ->din frontiera

        if problem.isGoalState(leaf.getState()):    #verific daca e goal state starea curenta si returnez lista de actiuni daca e valid
            while not(leaf.getParent() is None):
                actions.append(leaf.getAction())
                leaf = leaf.getParent()
            actions.reverse()
            return actions

        explored.append(leaf.getState())        #adaug starea curenta la lista de noduri explorate

        for succ in problem.getSuccessors(leaf.getState()):         #pentru fiecare succesor al nodului curent verific daca exista in lista de noduri explorate
            if not(explored.__contains__(succ[0])):                 #sau in frontiera
                newNode = Node(succ[0], succ[1], succ[2]+leaf.getCost(), leaf)
                for elem in frontier.heap:
                    if elem[2].getState() == newNode.getState():
                        ok = False                                      # ok <= false deoarece acesta se afla in frontiera
                        if elem[2].getCost() > newNode.getCost():       #daca nodul succesor creat se afla in frontiera dar costul sau este mai mic decat nodul din frontiera
                            frontier.heap.remove(elem)                  #inlocuiesc nodul din frontiera cu noul nod succesor
                            frontier.push(newNode, newNode.path_cost)
                        break
                if ok:
                    frontier.push(newNode, newNode.path_cost)           #daca ok == True atunci nodul nu se afla nici in frontiera si nici in lista de noduri explorate => il adaug in frontiera
            ok = True


def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """

    return 0


def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    frontier = util.PriorityQueue()         #initializam frontiera ca o coada de prioritati vida
    explored = []
    actions = []
    ok = True
    startState = Node(problem.getStartState(), None, 0, None)       #nodul starii de inceput
    i =0
    # tupla ce urmeaza a fi pusa in frontiera contine un nod cu informatiile despre acesta dar si functia  f (nod, f)
    # f = euristica  h + costul g
    startNode = (startState, heuristic(startState.getState(), problem)+0)

    frontier.push(startNode, startNode[1])
    while not frontier.isEmpty():       #cat timp frontiera nu e goala
        leaf = frontier.pop()           #scoatem un nod din frontiera

        if problem.isGoalState(leaf[0].getState()):     #daca e goal state atunci returnam lista de actiuni
            while not leaf[0].getParent() == None:
                actions.append(leaf[0].getAction())
                leaf = leaf[0].getParent()
            actions.reverse()
            return actions

        explored.append(leaf)   #adaugam nodul curent in lista de noduri explorate

        i+=1
        #pentru fiecare succesor al nodului curent
        for succ in problem.getSuccessors(leaf[0].getState()):

            newNode = Node(succ[0], succ[1], succ[2]+leaf[0].getCost(), leaf)
            #daca nu se afla in lista de noduri explorate
            if not(any(newNode.getState() == i[0].getState() for i in explored)):

                #creeam elementul ce contine (nod, f) -> f = h + g
                elem = (newNode, heuristic(newNode.getState(), problem) + newNode.getCost())

                #daca elementul nu se afla in frontiera il adaugam
                if not(any(newNode.getState() == i[2][0].getState() for i in frontier.heap)):
                    frontier.push(elem, elem[1])
                else:
                    # altfel daca se afla in frontiera dar costul elementului nou este mai mic decat cel existent in frontiera le \schimbam
                    for frontierElem in frontier.heap:
                        if frontierElem[2][0].getState() == newNode.getState() and frontierElem[2][1]>elem[1]:
                            frontier.heap.remove(frontierElem)
                            frontier.push(elem, elem[1])



def randomSearch(problem):
    actions = []
    stare = problem.getStartState()
    while not(problem.isGoalState(stare)):
        successors = problem.getSuccessors(stare)
        nr = len(successors)
        poz =int(random.random()*nr)
        stare = successors[poz][0]
        actions.append(successors[poz][1])
    print actions
    return actions

# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
rnds = randomSearch